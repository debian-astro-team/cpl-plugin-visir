/* $Id: visir_spc_optmod.h,v 1.19 2013-03-18 15:10:52 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-03-18 15:10:52 $
 * $Revision: 1.19 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_SPC_OPTMOD_H
#define VISIR_SPC_OPTMOD_H

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/* The size of the VISIR Optical Model object */
#define VISIR_SPC_OPTMOD_SIZEOF (4*sizeof(int) + 14 * sizeof(double))

/* The Central Wavelength [micron] for the LR Prism mode */
/* Computed by visir_spc_phys_lrp() */
#define VISIR_SPC_LRP_CWLEN 10.91

/* According to HUKaufl the resolution is approximately 300 */
#define VISIR_SPC_LRP_RESOL 300.

/* Dispersion at center [pixel/m] (Computed by visir_spc_phys_lrp()) */
#define VISIR_SPC_LRP_RESOL_DISP 121412177.

/* Wavelength on the 1'st pixel [m] (Computed by visir_spc_phys_lrp()) */
#define VISIR_SPC_LRP_WLEN0 6.70e-6

/* Wavelength on the last pixel [m] (Computed by visir_spc_phys_lrp()) */
#define VISIR_SPC_LRP_WLEN1 14.21e-6

#define VISIR_SPC_LRP_NAME "LRP"

/*-----------------------------------------------------------------------------
                                   New types
 -----------------------------------------------------------------------------*/

enum visir_spc_resol_ {
    VISIR_SPC_R_ERR = 0,
    VISIR_SPC_R_LRP,
    VISIR_SPC_R_LR,
    VISIR_SPC_R_MR,
    VISIR_SPC_R_HR,
    VISIR_SPC_R_GHR
};

typedef enum visir_spc_resol_ visir_spc_resol;

struct visir_optmod_ {
    /* For this struct only the size is public */
    /* The first member ensures proper alignment of this and its
       corresponding private struct, the second member ensures that
       the two structs have identical sizes */

    /* Any access to the below two members leads to undefined behaviour! */

    double ensure_alignment;
    char contents[VISIR_SPC_OPTMOD_SIZEOF - sizeof(double)];
};

typedef struct visir_optmod_ visir_optmod;

#undef VISIR_SPC_OPTMOD_SIZEOF

/*-----------------------------------------------------------------------------
                              Function prototypes
 -----------------------------------------------------------------------------*/

int    visir_spc_optmod_init(visir_spc_resol, double, visir_optmod *, int);
double visir_spc_optmod_wlen(const visir_optmod *, double *, double *);
double visir_spc_optmod_cross_dispersion(const visir_optmod *, double);
double visir_spc_optmod_echelle(const visir_optmod *, double, int);
double visir_spc_optmod_resolution(const visir_optmod *);
double visir_spc_optmod_dispersion(const visir_optmod *);
int    visir_spc_optmod_side_is_A(const visir_optmod *);
int    visir_spc_optmod_get_echelle_order(const visir_optmod *);

#endif

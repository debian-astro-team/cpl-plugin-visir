/* $Id: visir_cpl_compat.h,v 1.3 2013-05-13 16:02:43 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2013 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-05-13 16:02:43 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_CPL_COMPAT_H
#define VISIR_CPL_COMPAT_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

#if !defined CPL_VERSION_CODE || CPL_VERSION_CODE < CPL_VERSION(6, 0, 0)
/* cpl < 6, not supported by visir */
#error CPL too old, at least cpl 6 required
#endif

#if CPL_VERSION_CODE < CPL_VERSION(6, 1, 0)
#define CPL_FFT_FIND_MEASURE 0
#endif

#if CPL_VERSION_CODE < CPL_VERSION(6, 1, 0)
#define CPL_FITS_START_CACHING
#define CPL_FITS_RESTART_CACHING
#define CPL_FITS_STOP_CACHING
#define cpl_fits_set_mode(x)
#endif

#if CPL_VERSION_CODE < CPL_VERSION(6, 3, 0)
#define CPL_IO_COMPRESS_RICE 0
#endif


/*-----------------------------------------------------------------------------
                                       Function code
 -----------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------*/
/**
 * @brief get frame from frameset
 *
 * @param self     frameset
 * @param position index in frameset
 * @note only use if the implicit change current of position in frameset is
 *       not needed
 */
/* ---------------------------------------------------------------------------*/
static inline cpl_frame *
visir_frameset_get_frame(cpl_frameset *self, cpl_size position)
{
#if CPL_VERSION_CODE >= CPL_VERSION(6, 3, 0)
    return cpl_frameset_get_position(self, position);
#else
    return cpl_frameset_get_frame(self, position);
#endif
}


/* ---------------------------------------------------------------------------*/
/**
 * @brief get sum of vector
 *
 * @param vs vector to sum
 */
/* ---------------------------------------------------------------------------*/
static inline double
visir_vector_sum(const cpl_vector * vs)
{
#if CPL_VERSION_CODE >= CPL_VERSION(6, 3, 0)
    double sum = cpl_vector_get_sum(vs);
#else
    const size_t n = cpl_vector_get_size(vs);
    const double * data = cpl_vector_get_data_const(vs);
    double sum = 0;
    for (size_t j = 0; j < n; j++)
        sum += data[j];
#endif
    return sum;
}


/* ---------------------------------------------------------------------------*/
/**
 * @brief unwrap imagelist
 *
 * @param list imagelist
 */
/* ---------------------------------------------------------------------------*/
static inline void
visir_imagelist_unwrap(cpl_imagelist * list)
{
#if CPL_VERSION_CODE >= CPL_VERSION(6, 1, 0)
    cpl_imagelist_unwrap(list);
#else
    for (cpl_size i = cpl_imagelist_get_size(list); i-- > 0;)
        cpl_imagelist_unset(list, i);
    cpl_imagelist_delete(list);
#endif
}

#endif

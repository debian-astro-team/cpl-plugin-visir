/* $Id: visir_pfits.h,v 1.66 2013-09-24 10:46:00 jtaylor Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-09-24 10:46:00 $
 * $Revision: 1.66 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_PFITS_H
#define VISIR_PFITS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include "irplib_pfits.h"

#include <cpl.h>
#include <string.h>

/*-----------------------------------------------------------------------------
                                Define   
 -----------------------------------------------------------------------------*/

#define VISIR_PFITS_STRING_ARCFILE           "ARCFILE"
#define VISIR_PFITS_STRING_DATE_OBS          "DATE-OBS"
#define VISIR_PFITS_DOUBLE_DEC               "DEC"
#define VISIR_PFITS_DOUBLE_PIXSPACE          "ESO DET CHIP PXSPACE"
#define VISIR_PFITS_INT_CHOP_NCYCLES         "ESO DET CHOP NCYCLES"
#define VISIR_PFITS_DOUBLE_DIT               "ESO DET DIT"
#define VISIR_PFITS_DOUBLE_SEQ1_DIT          "ESO DET SEQ1 DIT"
#define VISIR_PFITS_INT_NAVRG                "ESO DET NAVRG"
#define VISIR_PFITS_STRING_FRAME_TYPE        "ESO DET FRAM TYPE"
#define VISIR_PFITS_STRING_MODE              "ESO DET MODE NAME"
#define VISIR_PFITS_INT_NDIT                 "ESO DET NDIT"
#define VISIR_PFITS_INT_NDITSKIP             "ESO DET NDITSKIP"
#define VISIR_PFITS_DOUBLE_VOLT1DCTA9        "ESO DET VOLT1 DCTA9"
#define VISIR_PFITS_DOUBLE_VOLT1DCTB9        "ESO DET VOLT1 DCTB9"
#define VISIR_PFITS_DOUBLE_VOLT2DCTA9        "ESO DET VOLT2 DCTA9"
#define VISIR_PFITS_DOUBLE_VOLT2DCTB9        "ESO DET VOLT2 DCTB9"
#define VISIR_PFITS_STRING_GRAT1_NAME        "ESO INS GRAT1 NAME"
#define VISIR_PFITS_DOUBLE_WLEN              "ESO INS GRAT1 WLEN"
#define VISIR_PFITS_DOUBLE_PWLEN             "ESO INS PRIS WLEN"
#define VISIR_PFITS_STRING_INSMODE           "ESO INS MODE"
#define VISIR_PFITS_DOUBLE_MONOC_POS         "ESO INS MONOC1 POS"
#define VISIR_PFITS_STRING_PIXSCALE          "ESO INS PFOV"
#define VISIR_PFITS_STRING_RESOL             "ESO INS RESOL"
#define VISIR_PFITS_STRING_SLITNAME          "ESO INS SLIT1 NAME"
#define VISIR_PFITS_INT_OBS_ID               "ESO OBS ID"
#define VISIR_PFITS_STRING_STARNAME          "ESO OBS TARG NAME"
#define VISIR_PFITS_STRING_CHOPNOD_DIR       "ESO SEQ CHOPNOD DIR"
#define VISIR_PFITS_DOUBLE_CUMOFFSETX        "ESO SEQ CUMOFFSETX"
#define VISIR_PFITS_DOUBLE_CUMOFFSETY        "ESO SEQ CUMOFFSETY"
#define VISIR_PFITS_STRING_NODPOS            "ESO SEQ NODPOS"
#define VISIR_PFITS_DOUBLE_AIRMASS_END       "ESO TEL AIRM END"
#define VISIR_PFITS_DOUBLE_AIRMASS_START     "ESO TEL AIRM START"
#define VISIR_PFITS_BOOL_CHOP_STATUS         "ESO TEL CHOP ST"
#define VISIR_PFITS_DOUBLE_CHOP_THROW        "ESO TEL CHOP THROW"
#define VISIR_PFITS_STRING_CHOP_START        "ESO TEL CHOP START"
#define VISIR_PFITS_DOUBLE_CHOP_FREQ         "ESO TEL CHOP FREQ"
#define VISIR_PFITS_DOUBLE_CHOP_POSANG       "ESO TEL CHOP POSANG"
#define VISIR_PFITS_STRING_CHOP_START        "ESO TEL CHOP START"
#define VISIR_PFITS_DOUBLE_ADA_POSANG        "ESO ADA POSANG"
#define VISIR_PFITS_STRING_OBS_START         "DATE-OBS"
#define VISIR_PFITS_DOUBLE_FOCUS             "ESO TEL FOCU LEN"
#define VISIR_PFITS_DOUBLE_ALPHA             "ESO TEL TARG OFFSETALPHA"
#define VISIR_PFITS_DOUBLE_DELTA             "ESO TEL TARG OFFSETDELTA"
#define VISIR_PFITS_DOUBLE_TEMP              "ESO TEL TH M1 TEMP"
#define VISIR_PFITS_INT_EXPNO                "ESO TPL EXPNO"
#define VISIR_PFITS_INT_NUMBEXP              "ESO TPL NEXP"
#define VISIR_PFITS_DOUBLE_EXPTIME           "EXPTIME"
#define VISIR_PFITS_STRING_INSTRUMENT        "INSTRUME"
#define VISIR_PFITS_DOUBLE_MJDOBS            "MJD-OBS"
#define VISIR_PFITS_INT_NAXIS3               "NAXIS3"
#define VISIR_PFITS_DOUBLE_RA                "RA"
#define VISIR_PFITS_DPR_TECH                 "ESO DPR TECH"

#define VISIR_PFITS_DOUBLE_SLITWIDTH         "ESO INS SLIT1 WID"
#define VISIR_PFITS_STRING_FILTER1           "ESO INS FILT1 NAME"
#define VISIR_PFITS_STRING_FILTER2           "ESO INS FILT2 NAME"

#define VISIR_DRS_CUMOFFSETX                 "ESO DRS CUMOFFSETX"
#define VISIR_DRS_CUMOFFSETY                 "ESO DRS CUMOFFSETY"
#define VISIR_DRS_CUMOFFSETXA                "ESO DRS CUMOFFSETXA"
#define VISIR_DRS_CUMOFFSETYA                "ESO DRS CUMOFFSETYA"
#define VISIR_DRS_CUMOFFSETXB                "ESO DRS CUMOFFSETXB"
#define VISIR_DRS_CUMOFFSETYB                "ESO DRS CUMOFFSETYB"

#define VISIR_PFITS_REGEXP_DIT \
        VISIR_PFITS_DOUBLE_DIT \
    "|" VISIR_PFITS_DOUBLE_SEQ1_DIT \
    "|" VISIR_PFITS_INT_CHOP_NCYCLES \
    "|" VISIR_PFITS_INT_NDIT \
    "|" VISIR_PFITS_INT_NDITSKIP \
    "|" VISIR_PFITS_INT_NAVRG

#define VISIR_PFITS_REGEXP_CAPA \
        VISIR_PFITS_STRING_INSMODE \
    "|" VISIR_PFITS_DOUBLE_VOLT1DCTA9 \
    "|" VISIR_PFITS_DOUBLE_VOLT1DCTB9 \
    "|" VISIR_PFITS_DOUBLE_VOLT2DCTA9 \
    "|" VISIR_PFITS_DOUBLE_VOLT2DCTB9

#define VISIR_PFITS_REGEXP_INPUTS_COMBINE       \
    VISIR_PFITS_REGEXP_DIT                      \
    "|" VISIR_PFITS_STRING_NODPOS               \
    "|" "ZNAXIS[12]"                            \
    "|" "NAXIS[12]"                             \
    "|"  VISIR_PFITS_REGEXP_LOAD_IMAGELIST

#define VISIR_PFITS_REGEXP_IMG_RECOMBINE \
    VISIR_PFITS_REGEXP_DIT  \
    "|" VISIR_PFITS_DOUBLE_CUMOFFSETX \
    "|" VISIR_PFITS_DOUBLE_CUMOFFSETY \
    "|" VISIR_PFITS_REGEXP_INPUTS_COMBINE \
    "|" VISIR_PFITS_STRING_CHOPNOD_DIR \
    "|" VISIR_PFITS_STRING_PIXSCALE    \
    "|" VISIR_PFITS_DOUBLE_CHOP_THROW  \
    "|" VISIR_PFITS_DOUBLE_CHOP_POSANG \
    "|" VISIR_PFITS_BOOL_CHOP_STATUS  \
    "|" VISIR_PFITS_DOUBLE_ADA_POSANG \
    "|" VISIR_PFITS_REGEXP_CAPA

#define VISIR_PFITS_REGEXP_SPC_GET_RES_WL \
         VISIR_PFITS_DOUBLE_WLEN \
    "|"  VISIR_PFITS_DOUBLE_SLITWIDTH \
    "|"  VISIR_PFITS_DOUBLE_PIXSPACE \
    "|"  VISIR_PFITS_STRING_RESOL \
    "|"  VISIR_PFITS_STRING_SLITNAME \
    "|"  VISIR_PFITS_DOUBLE_TEMP


/* Guaranteed keys of VISIR_PFITS_REGEXP_IMG_RECOMBINE */
#define VISIR_PFITS_REGEXP_SPC_SENSIT \
         VISIR_PFITS_INT_NDIT \
    "|"  VISIR_PFITS_DOUBLE_RA \
    "|"  VISIR_PFITS_DOUBLE_DEC \
    "|"  VISIR_PFITS_STRING_STARNAME


/* All recipes may use IRPLIB_PFITS_REGEXP_DPR */
#define VISIR_PFITS_REGEXP_LOAD_IMAGELIST \
    VISIR_PFITS_REGEXP_DIT  \
    "|"  VISIR_PFITS_STRING_FRAME_TYPE \
    "|"  VISIR_PFITS_INT_NAXIS3 \
    "|"  IRPLIB_PFITS_REGEXP_DPR


/* Guaranteed keys of VISIR_PFITS_REGEXP_IMG_RECOMBINE */
#define VISIR_PFITS_REGEXP_IMG_SENSIT \
         VISIR_PFITS_STRING_FILTER1 \
    "|"  VISIR_PFITS_STRING_FILTER2 \
    "|"  VISIR_PFITS_STRING_PIXSCALE \
    "|"  VISIR_PFITS_STRING_INSMODE \
    "|"  VISIR_PFITS_REGEXP_SPC_SENSIT


#define VISIR_PFITS_REGEXP_DARK_PAF             \
        IRPLIB_PFITS_REGEXP_PAF                 \
    "|" VISIR_PFITS_DOUBLE_DIT                  \
    "|" VISIR_PFITS_DOUBLE_SEQ1_DIT             \
    "|" VISIR_PFITS_INT_NDIT                    \
    "|" VISIR_PFITS_STRING_MODE                 \
    "|" VISIR_PFITS_STRING_PIXSCALE             \
    "|" VISIR_PFITS_STRING_RESOL                \
    "|ESO INS GRAT1 WLEN|ESO INS SLIT1 WID"     \
    "|ESO INS FILT1 NAME|ESO INS FILT2 NAME"    \
    "|ESO DET NCORRS NAME|ESO TPL START"        \
    "|" VISIR_PFITS_DOUBLE_PWLEN

/* All recipes may use IRPLIB_PFITS_REGEXP_DPR */
#define VISIR_PFITS_REGEXP_DARK                 \
        IRPLIB_PFITS_REGEXP_DPR                 \
    "|" VISIR_PFITS_DOUBLE_EXPTIME              \
    "|" "NAXIS[12]"

#define VISIR_PFITS_REGEXP_COMBINE_PAF      \
        IRPLIB_PFITS_REGEXP_PAF             \
  "|" VISIR_PFITS_DOUBLE_DIT                \
  "|" VISIR_PFITS_DOUBLE_SEQ1_DIT           \
  "|" VISIR_PFITS_STRING_PIXSCALE           \
  "|" VISIR_PFITS_DOUBLE_AIRMASS_START

#define VISIR_PFITS_REGEXP_IMG_PHOT_PAF     \
        IRPLIB_PFITS_REGEXP_PAF             \
       "|" VISIR_PFITS_DOUBLE_DIT \
        "|" VISIR_PFITS_DOUBLE_SEQ1_DIT     \
       "|" VISIR_PFITS_DOUBLE_AIRMASS_START \
       "|" VISIR_PFITS_DOUBLE_AIRMASS_END \
       "|" VISIR_PFITS_STRING_PIXSCALE

#define VISIR_PFITS_IMG_PHOT_COPY \
     IRPLIB_PFITS_REGEXP_RECAL    \
     "|FILTER1|FILTER2"

#define VISIR_PFITS_SPC_PHOT_COPY \
     VISIR_PFITS_IMG_PHOT_COPY "|GRAT1|PRIS"

#define VISIR_PFITS_FF_COPY \
     VISIR_PFITS_SPC_PHOT_COPY

#define VISIR_PFITS_REGEXP_SPC_WCAL_PAF         \
        IRPLIB_PFITS_REGEXP_PAF                 \
    "|" VISIR_PFITS_STRING_GRAT1_NAME

#define VISIR_PFITS_REGEXP_SPC_PHOT_PAF         \
        VISIR_PFITS_REGEXP_IMG_PHOT_PAF         \
    "|" VISIR_PFITS_STRING_GRAT1_NAME

/* Together with the keys referenced above,
   no other FITS keys are used by the VISIR pipeline */
static const char visir_property_regexp[] = 
    "^(" IRPLIB_PFITS_REGEXP_PAF
    "|" VISIR_PFITS_SPC_PHOT_COPY
    "|" VISIR_PFITS_STRING_ARCFILE
    "|" VISIR_PFITS_STRING_DATE_OBS
    "|" VISIR_PFITS_DOUBLE_DEC
    "|" VISIR_PFITS_DOUBLE_PIXSPACE
    "|" VISIR_PFITS_INT_CHOP_NCYCLES
    "|" VISIR_PFITS_DOUBLE_DIT
    "|" VISIR_PFITS_DOUBLE_SEQ1_DIT
    "|" VISIR_PFITS_STRING_FRAME_TYPE
    "|" VISIR_PFITS_STRING_MODE
    "|" VISIR_PFITS_INT_NDIT
    "|" VISIR_PFITS_DOUBLE_VOLT1DCTA9
    "|" VISIR_PFITS_DOUBLE_VOLT1DCTB9
    "|" VISIR_PFITS_DOUBLE_VOLT2DCTA9
    "|" VISIR_PFITS_DOUBLE_VOLT2DCTB9
    "|" VISIR_PFITS_STRING_GRAT1_NAME
    "|" VISIR_PFITS_DOUBLE_WLEN
    "|" VISIR_PFITS_STRING_INSMODE
    "|" VISIR_PFITS_DOUBLE_MONOC_POS
    "|" VISIR_PFITS_STRING_PIXSCALE
    "|" VISIR_PFITS_STRING_RESOL
    "|" VISIR_PFITS_STRING_SLITNAME
    "|" VISIR_PFITS_DOUBLE_SLITWIDTH
    "|" VISIR_PFITS_INT_OBS_ID
    "|" VISIR_PFITS_STRING_STARNAME
    "|" VISIR_PFITS_STRING_CHOPNOD_DIR
    "|" VISIR_PFITS_BOOL_CHOP_STATUS
    "|" VISIR_PFITS_DOUBLE_CHOP_THROW
    "|" VISIR_PFITS_DOUBLE_CHOP_POSANG
    "|" VISIR_PFITS_DOUBLE_ADA_POSANG
    "|" VISIR_PFITS_DOUBLE_CUMOFFSETX
    "|" VISIR_PFITS_DOUBLE_CUMOFFSETY
    "|" VISIR_PFITS_STRING_NODPOS
    "|" VISIR_PFITS_DOUBLE_AIRMASS_END
    "|" VISIR_PFITS_DOUBLE_AIRMASS_START
    "|" VISIR_PFITS_DOUBLE_FOCUS
    "|" VISIR_PFITS_DOUBLE_ALPHA
    "|" VISIR_PFITS_DOUBLE_DELTA
    "|" VISIR_PFITS_DOUBLE_TEMP
    "|" VISIR_PFITS_INT_EXPNO
    "|" VISIR_PFITS_INT_NUMBEXP
    "|" VISIR_PFITS_DOUBLE_EXPTIME
    "|" VISIR_PFITS_STRING_INSTRUMENT
    "|" VISIR_PFITS_DOUBLE_MJDOBS
    "|" "NAXIS[12]"
    "|" VISIR_PFITS_INT_NAXIS3
    "|" VISIR_PFITS_DOUBLE_RA
    "|" VISIR_PFITS_STRING_FILTER1
    "|" VISIR_PFITS_STRING_FILTER2
    "|" IRPLIB_PFITS_REGEXP_DPR ")$";

/*-----------------------------------------------------------------------------
                                   Functions prototypes
 -----------------------------------------------------------------------------*/

static inline cpl_boolean visir_is_spc(const cpl_propertylist * plist)
{
    const char * tech;

    if (!cpl_propertylist_has(plist, VISIR_PFITS_DPR_TECH)) {
        return CPL_FALSE;
    }
    tech = cpl_propertylist_get_string(plist, VISIR_PFITS_DPR_TECH);
    if (!tech) {
        return CPL_FALSE;
    }
    if (strstr(tech, "SPECTRUM") || strstr(tech, "ECHELLE")) {
        return CPL_TRUE;
    }
    return CPL_FALSE;
}

static inline cpl_boolean visir_is_img(const cpl_propertylist * plist)
{
    const char * tech;

    if (!cpl_propertylist_has(plist, VISIR_PFITS_DPR_TECH)) {
        return CPL_FALSE;
    }
    tech = cpl_propertylist_get_string(plist, VISIR_PFITS_DPR_TECH);
    if (!tech) {
        return CPL_FALSE;
    }
    if (strstr(tech, "IMAGE")) {
        return CPL_TRUE;
    }
    return CPL_FALSE;
}

double visir_pfits_get_airmass_start(const cpl_propertylist *);
double visir_pfits_get_airmass_end(const cpl_propertylist *);
double visir_pfits_get_alpha(const cpl_propertylist *);
const char * visir_pfits_get_arcfile(const cpl_propertylist *);
const char * visir_pfits_get_chopnod_dir(const cpl_propertylist *);
int visir_pfits_get_chop_ncycles(const cpl_propertylist *);
double visir_pfits_get_chop_throw(const cpl_propertylist *);
double visir_pfits_get_chop_stat(const cpl_propertylist * self);
double visir_pfits_get_chop_freq(const cpl_propertylist * self);
double visir_pfits_get_chop_posang(const cpl_propertylist *);
double visir_pfits_get_chop_pthrow(const cpl_propertylist *);
double visir_pfits_get_ada_posang(const cpl_propertylist *);
double visir_pfits_get_cumoffsetx(const cpl_propertylist *);
double visir_pfits_get_cumoffsety(const cpl_propertylist *);
const char * visir_pfits_get_date_obs(const cpl_propertylist *);
double visir_pfits_get_delta(const cpl_propertylist *);
double visir_pfits_get_dec(const cpl_propertylist *);
double visir_pfits_get_dit(const cpl_propertylist *);
int visir_pfits_get_navrg(const cpl_propertylist *);
int visir_pfits_get_expno(const cpl_propertylist *);
const char * visir_pfits_get_filter(const cpl_propertylist *);
double visir_pfits_get_focus(const cpl_propertylist *);
double visir_pfits_get_exptime(const cpl_propertylist *);
const char * visir_pfits_get_frame_type(const cpl_propertylist *);
const char * visir_pfits_get_det_name(const cpl_propertylist *);
const char * visir_pfits_get_grat1_name(const cpl_propertylist *);
const char * visir_pfits_get_insmode(const cpl_propertylist *);
const char * visir_pfits_get_instrument(const cpl_propertylist *);
double visir_pfits_get_mjdobs(const cpl_propertylist *);
const char * visir_pfits_get_mode(const cpl_propertylist *);
double visir_pfits_get_monoc_pos(const cpl_propertylist *);
int visir_pfits_get_ndit(const cpl_propertylist *);
int visir_pfits_get_naxis1(const cpl_propertylist *);
int visir_pfits_get_naxis2(const cpl_propertylist *);
int visir_pfits_get_naxis3(const cpl_propertylist *);
int visir_pfits_get_win_nx(const cpl_propertylist * self);
int visir_pfits_get_win_ny(const cpl_propertylist * self);
int visir_pfits_get_start_x(const cpl_propertylist * self);
int visir_pfits_get_start_y(const cpl_propertylist * self);
int visir_pfits_get_numbexp(const cpl_propertylist *);
int visir_pfits_get_obs_id(const cpl_propertylist *);
const char * visir_pfits_get_nodpos(const cpl_propertylist *);
double visir_pfits_get_pixscale(const cpl_propertylist *);
double visir_pfits_get_pixspace(const cpl_propertylist *);
double visir_pfits_get_ra(const cpl_propertylist *);
double visir_pfits_get_slitwidth(const cpl_propertylist *);
const char * visir_pfits_get_starname(const cpl_propertylist *);
const char * visir_pfits_get_resol(const cpl_propertylist *);
double visir_pfits_get_temp(const cpl_propertylist *);
double visir_pfits_get_volt1dcta9(const cpl_propertylist *);
double visir_pfits_get_volt1dctb9(const cpl_propertylist *);
double visir_pfits_get_volt2dcta9(const cpl_propertylist *);
double visir_pfits_get_volt2dctb9(const cpl_propertylist *);
double visir_pfits_get_wlen(const cpl_propertylist *);
double visir_pfits_get_img_weight(const cpl_propertylist * );
int visir_pfits_get_nbeams(const cpl_propertylist * );

#endif 

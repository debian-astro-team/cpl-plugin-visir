/* $Id: visir_serialize.c,v 1.2 2013-01-15 11:04:42 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2012 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-01-15 11:04:42 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "visir_utils.h"

#include <cpl.h>

#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include <arpa/inet.h>

typedef struct {
    size_t size;
    char * base_buffer;
    char * pos;
} visir_stream;

static visir_stream *
visir_stream_new(char * block, size_t size)
{
    visir_stream * stream = cpl_malloc(sizeof(visir_stream));
    stream->size = size;
    stream->base_buffer = block;
    stream->pos = stream->base_buffer;
    return stream;
}

static size_t
visir_stream_get_size(visir_stream * stream)
{
    return stream->pos - stream->base_buffer;
}

static void
visir_stream_ensure_space(visir_stream * stream, size_t extra)
{
    const size_t nused = visir_stream_get_size(stream);

    if (stream->size < (nused + extra)) {
        stream->base_buffer = cpl_realloc(stream->base_buffer,
                                          stream->size * 2);
        stream->size *= 2;
        stream->pos = stream->base_buffer + nused;
    }
}

static void
serialize_uint32(visir_stream * stream, uint32_t v_)
{
    uint32_t v = htonl(v_);

    visir_stream_ensure_space(stream, sizeof(v));
    memcpy(stream->pos, &v, sizeof(v));
    stream->pos += sizeof(v);
}

static void
serialize_string(visir_stream * stream, const char * s)
{
    size_t n = strlen(s);
    serialize_uint32(stream, n);
    visir_stream_ensure_space(stream, n);
    memcpy(stream->pos, s, n);
    stream->pos += n;
}


static uint32_t
deserialize_uint32(visir_stream * stream)
{
    uint32_t r;
    memcpy(&r, stream->pos, sizeof(r));
    stream->pos += sizeof(r);
    return ntohl(r);
}

static char *
deserialize_string(visir_stream * stream)
{
    uint32_t n = deserialize_uint32(stream);
    char * s = cpl_malloc(n + 1);
    memcpy(s, stream->pos, n);
    s[n] = 0;
    stream->pos += n;
    return s;
}

char *
visir_frameset_serialize(const cpl_frameset * frames, size_t * size)
{
    char * block = NULL;
    visir_stream * stream = NULL;

    cpl_ensure(size != NULL, CPL_ERROR_NULL_INPUT, NULL);
    cpl_ensure(frames != NULL, CPL_ERROR_NULL_INPUT, NULL);

    block = cpl_malloc(1000);
    stream = visir_stream_new(block, 1000);

    serialize_uint32(stream, cpl_frameset_get_size(frames));
    FOR_EACH_FRAMESET_C(frm, frames) {
        serialize_uint32(stream, cpl_frame_get_type(frm));
        serialize_uint32(stream, cpl_frame_get_group(frm));
        serialize_uint32(stream, cpl_frame_get_level(frm));
        serialize_string(stream, cpl_frame_get_tag(frm));
        serialize_string(stream, cpl_frame_get_filename(frm));
    }

    *size = visir_stream_get_size(stream);
    block = stream->base_buffer;
    cpl_free(stream);

    return block;
}

cpl_frameset *
visir_frameset_deserialize(char * block, size_t * size)
{
    visir_stream * stream = visir_stream_new(block, 0);
    uint32_t nframes = deserialize_uint32(stream);
    cpl_frameset * frames = cpl_frameset_new();

    for (uint32_t i = 0; i < nframes; i++) {
        uint32_t type = deserialize_uint32(stream);
        uint32_t group = deserialize_uint32(stream);
        uint32_t level = deserialize_uint32(stream);
        char * tag = deserialize_string(stream);
        char * filename = deserialize_string(stream);
        cpl_frame * frm = cpl_frame_new();
        cpl_frame_set_type(frm, type);
        cpl_frame_set_group(frm, group);
        cpl_frame_set_level(frm, level);
        cpl_frame_set_filename(frm, filename);
        cpl_frame_set_tag(frm, tag);
        cpl_free(filename);
        cpl_free(tag);
        cpl_frameset_insert(frames, frm);
    }

    if (size)
        *size = visir_stream_get_size(stream);

    cpl_free(stream);
    return frames;
}

/* convinience function to send framset and error code over a stream */
cpl_error_code
visir_send_frameset(FILE * stream, const cpl_frameset * frames)
{
    size_t size;
    char * block = visir_frameset_serialize(frames, &size);
    cpl_error_code err = cpl_error_get_code();

    skip_if(fwrite(&err, sizeof(err), 1, stream) != 1);
    skip_if(fwrite(&size, sizeof(size), 1, stream) != 1);
    skip_if(fwrite(block, size, 1, stream) != 1);

    end_skip;

    cpl_free(block);
    return cpl_error_get_code();
}

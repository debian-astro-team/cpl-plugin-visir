/*
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003,2015,2016 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef VISIR_SPECTRO_H
#define VISIR_SPECTRO_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include "visir_spc_optmod.h"
#include "visir_inputs.h"
#include "irplib_framelist.h"
#include <cpl.h>
#include <stdbool.h>

/* approximate pixel position of 8um and 13 um on aquarius */
#define VISIR_AQU_APPROX_WLEN8 140
#define VISIR_AQU_APPROX_WLEN13 840

typedef struct {
    const char * recipename;
    const cpl_parameterlist * parlist;
    cpl_boolean do_fixcombi;
    cpl_propertylist * phu;

    /* Inputs */
    int          auto_bpm;
    int          plot;
    double       phi;
    double       ksi;
    double       eps;
    double       delta;
    double       gain;
    double       ron;
    double       ox_sigma;
    int          ox_niters;
    int          ox_smooth;
    int          ox_kernel;
    int          bkgcorrect;
    const char * respcal;

    /* only non zero for echelle */
    int         orderoffset;
    size_t      extract;

    /* photometry */
    double      phot_emis_tol;
} visir_spc_config;

visir_spc_resol visir_spc_get_res_wl(const irplib_framelist *, double *,
                                     double *, double *, double *, int);

cpl_image * visir_spc_flip(const cpl_image *, double, visir_spc_resol,
                           visir_data_type, bool *);

cpl_bivector * visir_bivector_load_fits(const char *, const char *,
                                        const char *, int);
cpl_error_code visir_vector_resample(cpl_vector *, const cpl_vector *,
                                        const cpl_bivector *);
void * visir_spc_extract_wcal(const cpl_image *, const cpl_image *,
                              const int, const int, const double, const double,
                              const double, const double, const visir_spc_resol,
                              const visir_spc_config *, const char *,
                              const char *, const int, const visir_apdefs *,
                              const cpl_size, const bool,
                              cpl_table **, cpl_image **, cpl_propertylist *);

cpl_error_code visir_spc_wavecal(const cpl_image *,
                                 cpl_propertylist *,
                                 double, double, double,
                                 double, visir_spc_resol,
                                 const visir_spc_config *,
                                 const char *,
                                 const char *,
                                 cpl_table **, int);

cpl_error_code visir_spectro_qc(cpl_propertylist *, cpl_propertylist *,
                                cpl_boolean, const irplib_framelist *,
                                const char *, const char *);

cpl_error_code visir_spc_echelle_limit(int *, int *, double,
                                       const visir_spc_config * cfg,
                                       int, int, int);
cpl_image * visir_spc_column_extract(const cpl_image *, int, int, int);


cpl_error_code
visir_spc_extract_order(cpl_image ** order,
                        cpl_image ** comorder,
                        int * lcol, int * rcol,
                        const cpl_image * combined,
                        const cpl_image * imhcycle,
                        const double wlen,
                        const visir_spc_config * pconfig,
                        const cpl_boolean do_ech,
                        const int);

int visir_norm_coord(const bool rev, const float coord, const int lcol,
                     const int rcol, const visir_apdefs * aps);
#endif

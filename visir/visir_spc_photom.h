/* $Id: visir_spc_photom.h,v 1.21 2012-08-21 09:56:14 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2012-08-21 09:56:14 $
 * $Revision: 1.21 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VISIR_SPC_PHOTOM_H
#define VISIR_SPC_PHOTOM_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "visir_spectro.h"
#include "visir_pfits.h"
#include "visir_utils.h"

/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                       Prototypes
 -----------------------------------------------------------------------------*/

cpl_error_code
visir_spc_phot_sensit(const irplib_framelist * rawframes,
                      const visir_spc_config * pconfig,
                      const cpl_propertylist * plist,
                      const char   * star_cat,
                      cpl_image   ** pweight2d,
                      cpl_propertylist * qclist,
                      cpl_table *   spc_table,
                      const visir_spc_resol resol,
                      const char * dit_key);

cpl_table *
visir_spc_phot_sensit_from_image(cpl_image **, const irplib_framelist *,
                                 const visir_spc_config *, const char *,
                                 const char *, const char *, cpl_image **,
                                 cpl_propertylist * qclist,
                                 cpl_boolean, double , double, double, double,
                                 const visir_spc_resol,
                                 const char * dit_key);

#endif

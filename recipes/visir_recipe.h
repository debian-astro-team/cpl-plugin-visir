/* $Id: visir_recipe.h,v 1.23 2013-02-27 09:04:24 jtaylor Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-02-27 09:04:24 $
 * $Revision: 1.23 $
 * $Name: not supported by cvs2svn $
 */



/* include file for VISIR recipes */
/* cpp macros that must be defined before inclusion:

   RECIPE_NAME:         e.g. visir_img_combine
   RECIPE_STRING:       e.g. "visir_img_combine"
   RECIPE_SYNOPSIS:     String with the recipe synopsis
   RECIPE_DESCRIPTION:  String with the recipe descrption
   Optionally:
   RECIPE_PARAMS:       Binary or of VISIR parameter enums

*/


#ifndef VISIR_RECIPE_H
#define VISIR_RECIPE_H

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/


#include "irplib_plugin.h"

#include "visir_parameter.h"
#include "visir_utils.h"
#include "visir_pfits.h"
#include "visir_dfs.h"
#include "visir_inputs.h"
#include "visir_cpl_compat.h"
#include <cpl.h>

#include <math.h>
#include <assert.h>
#include <float.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

#define visir_pipe_id PACKAGE "/" PACKAGE_VERSION

#define VISIR_RECIPE_DEFINE(RECIPE_NAME, RECIPE_PARAMS, SYNOPSIS, DESCRIPTION) \
    cpl_recipe_define(RECIPE_NAME, VISIR_BINARY_VERSION,                \
                      "Lars Lundin", PACKAGE_BUGREPORT, "2004, 2005",   \
                      SYNOPSIS, DESCRIPTION);                           \
                                                                        \
    static cpl_error_code CPL_CONCAT2X(RECIPE_NAME,fill_parameterlist)  \
        (cpl_parameterlist * self) {                                    \
        return visir_parameter_set(self, #RECIPE_NAME, RECIPE_PARAMS)   \
            ? cpl_error_set_where(cpl_func) : CPL_ERROR_NONE;           \
    }                                                                   \
    extern int CPL_CONCAT2X(RECIPE_NAME,plugin_end)
    
#endif

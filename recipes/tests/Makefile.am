## Process this file with automake to produce Makefile.in

##   This file is part of the VISIR Pipeline Library
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~


if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif

LIBVISIR = $(top_builddir)/visir/libvisir.la \
           $(top_builddir)/irplib/libirplib.la

OBJDIR = $(top_builddir)/recipes/.libs

AM_CPPFLAGS = $(all_includes)

noinst_HEADERS = visir_recipes_test.h

EXTRA_DIST = visir_recipes_test.c

LDADD = $(LIBVISIR) $(LIBCEXT)

check_PROGRAMS = visir_old_spc_wcal-test visir_old_spc_wcal_ech-test \
                 visir_old_spc_obs-test visir_old_spc_obs_ech-test \
                 visir_old_spc_phot-test visir_old_spc_phot_ech-test \
                 visir_util_img_std_cat-test \
                 visir_util_spc_std_cat-test \
                 visir_util_spc_txt2fits-test \
                 visir_util_repack-test \
                 visir_util_convert_weight-test \
                 visir_util_join-test \
                 visir_util_clip-test \
                 visir_util_run_swarp-test \
                 visir_util_undistort-test \
                 visir_old_util_destripe-test \
		 visir_util_detect_shift-test \
		 visir_util_qc-test \
		 visir_util_apply_calib-test \
                 visir_img_trans-test \
                 visir_img_ff-test visir_old_img_phot-test \
                 visir_img_dark-test \
                 visir_old_img_combine-test

visir_old_spc_wcal_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_spc_wcal_test_LDADD   = $(OBJDIR)/visir_spc_wcal.o $(LDADD) $(LIBALL)

visir_old_spc_wcal_ech_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_spc_wcal_ech_test_LDADD   = $(OBJDIR)/visir_spc_wcal_ech.o $(LDADD) $(LIBALL)

visir_old_spc_obs_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_spc_obs_test_LDADD   = $(OBJDIR)/visir_spc_obs.o $(LDADD) $(LIBALL)

visir_old_spc_obs_ech_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_spc_obs_ech_test_LDADD   = $(OBJDIR)/visir_spc_obs_ech.o $(LDADD) $(LIBALL)

visir_old_spc_phot_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_spc_phot_test_LDADD   = $(OBJDIR)/visir_spc_phot.o $(LDADD) $(LIBALL)

visir_old_spc_phot_ech_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_spc_phot_ech_test_LDADD   = $(OBJDIR)/visir_spc_phot_ech.o $(LDADD) $(LIBALL)

visir_util_img_std_cat_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_img_std_cat_test_LDADD   = $(OBJDIR)/visir_util_img_std_cat.o $(LDADD) $(LIBALL)

visir_util_spc_std_cat_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_spc_std_cat_test_LDADD   = $(OBJDIR)/visir_util_spc_std_cat.o $(LDADD) $(LIBALL)

visir_util_spc_txt2fits_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_spc_txt2fits_test_LDADD   = $(OBJDIR)/visir_util_spc_txt2fits.o $(LDADD) $(LIBALL)

visir_util_repack_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_repack_test_LDADD   = $(OBJDIR)/visir_util_repack.o $(LDADD) $(LIBALL)

visir_util_convert_weight_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_convert_weight_test_LDADD   = $(OBJDIR)/visir_util_convert_weight.o $(LDADD) $(LIBALL)

visir_util_clip_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_clip_test_LDADD   = $(OBJDIR)/visir_util_clip.o $(LDADD) $(LIBALL)

visir_util_undistort_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_undistort_test_LDADD   = $(OBJDIR)/visir_util_undistort.o $(LDADD) $(LIBALL)

visir_old_util_destripe_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_util_destripe_test_LDADD   = $(OBJDIR)/visir_util_destripe.o $(LDADD) $(LIBALL)

visir_util_detect_shift_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_detect_shift_test_LDADD   = $(OBJDIR)/visir_util_detect_shift.o $(LDADD) $(LIBALL)

visir_util_join_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_join_test_LDADD   = $(OBJDIR)/visir_util_join.o $(LDADD) $(LIBALL)

visir_util_qc_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_qc_test_LDADD   = $(OBJDIR)/visir_util_qc.o $(LDADD) $(LIBALL)

visir_util_apply_calib_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_apply_calib_test_LDADD   = $(OBJDIR)/visir_util_apply_calib.o $(LDADD) $(LIBALL)

visir_util_run_swarp_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_util_run_swarp_test_LDADD   = $(OBJDIR)/visir_util_run_swarp.o $(LDADD) $(LIBALL)

visir_old_img_combine_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_img_combine_test_LDADD   = $(OBJDIR)/visir_img_combine.o $(LDADD) $(LIBALL)

visir_img_dark_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_img_dark_test_LDADD   = $(OBJDIR)/visir_img_dark.o $(LDADD) $(LIBALL)

visir_img_ff_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_img_ff_test_LDADD   = $(OBJDIR)/visir_img_ff.o $(LDADD) $(LIBALL)

visir_old_img_phot_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_old_img_phot_test_LDADD   = $(OBJDIR)/visir_img_phot.o $(LDADD) $(LIBALL)

visir_img_trans_test_SOURCES = recipe_main.c visir_recipes_test.c
visir_img_trans_test_LDADD   = $(OBJDIR)/visir_img_trans.o $(LDADD) $(LIBALL)

# Be sure to reexport important environment variables.
# - And be sure the find the dynamic libraries
TESTS_ENVIRONMENT = MAKE="$(MAKE)" CC="$(CC)" CFLAGS="$(CFLAGS)" \
        CPPFLAGS="$(CPPFLAGS)" LD="$(LD)" LDFLAGS="$(LDFLAGS)" \
        LIBS="$(LIBS)" LN_S="$(LN_S)" NM="$(NM)" RANLIB="$(RANLIB)" \
        LD_LIBRARY_PATH="$(top_builddir)/visir/.libs:$(top_builddir)/irplib/.libs:$(LD_LIBRARY_PATH)" \
        MALLOC_PERTURB_=31 MALLOC_CHECK_=$(ESO_MALLOC_CHECK) \
        OBJEXT="$(OBJEXT)" EXEEXT="$(EXEEXT)"

TESTS = $(check_PROGRAMS)

# We need to remove any files that the above tests created.
clean-local:
	$(RM) *.paf *.fits *.log .logfile

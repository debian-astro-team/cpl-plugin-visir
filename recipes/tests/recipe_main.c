/* $Id: recipe_main.c,v 1.8 2011-10-05 10:09:50 llundin Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2011-10-05 10:09:50 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include "visir_recipes_test.h"

#include <irplib_plugin.h>



/*----------------------------------------------------------------------------*/
/**
 * @defgroup recipe_main   General plugin tests
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
                            Function definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Find a plugin and submit it to some tests
  @return   0 iff succesful

 */
/*----------------------------------------------------------------------------*/
int main(void)
{

    const char * tags[] = {
        "ERROR_TAG",
        "IMG_STD_CATALOG",
        "SPEC_CAL_QEFF",
        "SPEC_CAL_LINES",
        "SPEC_STD_CATALOG",
        "CALIB",
        "CALIB",
        "CALIB",
        "CALIB",
        "CALIB",
        "CALIB",
        "CALIB",
        "CALIB",
        "RRRECIPE_DOCATG_RAW",
        "RRRECIPE_DOCATG_RAW",
        "RRRECIPE_DOCATG_RAW",
        "IIINSTRUMENT_DOCATG_FLAT",
        "IIINSTRUMENT_DOCATG_FLAT",
        "IIINSTRUMENT_DOCATG_FLAT",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "RAW",
        "PRO_MASTER_DARK",
        "PRO_MASTER_DARK",
        "PRO_MASTER_DARK",
        "CUBE2",
        "CUBE2",
        "CUBE2",
        "PRO_OBJECT_NODDING_STACKED",
        "PRO_OBJECT_NODDING_STACKED",
        "PRO_OBJECT_NODDING_STACKED",
        "CAL_DARK",
        "CAL_DARK",
        "CAL_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_TW",
        "IM_JITTER_OBJ",
        "IM_JITTER_OBJ",
        "IM_JITTER_OBJ",
        "IM_CAL_ACHRO",
        "IM_CAL_ACHRO",
        "IM_CAL_ACHRO",
        "IM_OBS_CHO_NOD_JIT",
        "IM_OBS_CHO_NOD_JIT",
        "IM_OBS_CHO_NOD_JIT",
        "IM_OBS_CHO_JIT",
        "IM_OBS_CHO_JIT",
        "IM_OBS_CHO_JIT",
        "IM_OBS_NOD_JIT",
        "IM_OBS_NOD_JIT",
        "IM_OBS_NOD_JIT",
        "IM_OBS_DIR_JIT",
        "IM_OBS_DIR_JIT",
        "IM_OBS_DIR_JIT",
        "IM_CAL_FLAT",
        "IM_CAL_FLAT",
        "IM_CAL_FLAT",
        "IM_TEC_FOCUS",
        "IM_TEC_FOCUS",
        "IM_TEC_FOCUS",
        "IM_CAL_ILLU",
        "IM_CAL_ILLU",
        "IM_CAL_ILLU",
        "IM_CAL_PFOV_BIN",
        "IM_CAL_PFOV_BIN",
        "IM_CAL_PFOV_BIN",
        "IM_CAL_PFOV_TEL",
        "IM_CAL_PFOV_TEL",
        "IM_CAL_PFOV_TEL",
        "IM_CAL_PHOT",
        "IM_CAL_PHOT",
        "IM_CAL_PHOT",
        "IM_CAL_PSF",
        "IM_CAL_PSF",
        "IM_CAL_PSF",
        "IM_TEC_TRANS",
        "IM_TEC_TRANS",
        "IM_TEC_TRANS",
        "SPEC_OBS_HRG",
        "SPEC_OBS_HRG",
        "SPEC_OBS_HRG",
        "SPEC_OBS_LMR",
        "SPEC_OBS_LMR",
        "SPEC_OBS_LMR",
        "SPEC_CAL_PHOT",
        "SPEC_CAL_PHOT",
        "SPEC_CAL_PHOT",
        "SPEC_CAL_HRG_WCAL",
        "SPEC_CAL_HRG_WCAL",
        "SPEC_CAL_HRG_WCAL",
        "SPEC_CAL_LMR_WCAL",
        "SPEC_CAL_LMR_WCAL",
        "SPEC_CAL_LMR_WCAL",
        "SPC_OBS",
        "SPC_OBS",
        "SPC_OBS"
    };

    cpl_pluginlist * pluginlist;
    const size_t ntags = sizeof(tags) / sizeof(char*);

    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    pluginlist = cpl_pluginlist_new();

    cpl_test_zero(cpl_plugin_get_info(pluginlist));

    cpl_test_zero(irplib_plugin_test(pluginlist, ntags, tags));
    if (getenv("RECIPE_UNIT_TEST") != NULL &&
        cpl_pluginlist_find(pluginlist, "visir_util_clip")) {
        cpl_msg_warning(cpl_func, "Extra test 1 of: visir_util_clip");
        visir_util_clip_test_one(pluginlist);
    }

    cpl_pluginlist_delete(pluginlist);

    return cpl_test_end(0);
}

/**@}*/
